\section{Introduction}

Equality and computation are central components of type theories.
The computational content of a type theory is presented by elimination rules (often called $\beta$-rules), and perhaps uniqueness rules (usually called $\eta$-rules) or more exotic rules (such as the $\nu$-rules considered in \cite{NewEqsNeutrals}).
This computational content is typically explained by the means of a normalization algorithm.
In presence of identity types, there is a distinction between two notions of equality between terms of a type theory.
Internally, the identity types provide the notion of \emph{internal equality}, also often called \emph{propositional equality}, or sometimes \emph{typal equality} to emphasize that it does not have to be truncated.
Externally, we can also compare terms up to \emph{strict equality}, which is the proof-irrelevant equality of our metatheory.
Strict equality is also often called \emph{definitional} or \emph{judgemental} equality.

When working internally to a type theory, it is desirable to have as many strict equalities as possible.
Indeed, equalities that hold strictly are equalities that can be implicitly and silently coerced over.
On the other hand, equalities that are only internal require explicit transports and coercions, which quickly clutter the terms of the theory.
Conversely, the trade-off is that type theories with additional strict equalities have fewer models, and their semantics are therefore more complicated.

Hofmann proved in \cite{HofmannCons} a conservativity theorem, showing that all equalities in a type theory can conservatively be made strict, in the presence of enough extensionality principles in the base type theory.
The most important of these extensionality principles is the Uniqueness of Identity Proofs (UIP) principle, which states that any two proofs of a weak equality are themselves weakly equal.
A more syntactic proof was later given by Oury~\cite{OuryConservativity} for the calculus of constructions.
Oury's proof had some issues, mainly due to a presentation of the syntax of type theory with too few annotations.
An improvement of Oury's proof and a presentation of this result as a constructive and effective syntactic translation has been given recently by Winterhalter et al~\cite{ElimRefl}.

Since Hofmann's proof of conservativity, there has been a lot of interest going into the study of type theories with non-trivial higher dimensional content inconsitent with UIP~\cite{GroupoidModel}, and their semantics in homotopy theoretic~\cite{HomotopyTheoreticModels, SimplicialModel} and $\infty$-categorical structures~\cite{LangLexInftyCats}.
For type theories without UIP, strict equalities are even more important, because they are automatically coherent.
Thus having more strict equalities means that we escape not only ``transport hell'', but also ``higher-dimensional transport and coherence hell''.
Conversely, it is in practice much harder to justify strict equalities in many homotopy theoretic models.
Some authors have even considered weak variants of the basic computation rules of type theories.
For instance, weak identity types, whose computation rule only holds up to internal equality, have been introduced by \cite{PropIdTypes}, under the name of propositional identity types.
The path types of cubical type theories~\cite{CTT} also only satisfy weakly the computation rule of identity types.
Other type structures can be weakened similarly, and we can even consider type theories whose computation rules are all expressed by internal equalities instead of strict equalities.
At the level of types and universes, instead of assuming that each type former is strictly classified by some code in the universe, we can ask for them to be classified only up to type equivalence.
These weak Tarski universes have been introduced in \cite{WeakTarski}.
The fact that homotopy type theory with strict univalent universes, rather than weak universes, can be interpreted in every $(\infty, 1)$-topos has only been established recently~\cite{InftyToposesUniverses}.

In this setting, we can wonder how type theories with varying amounts of strict equalities can be compared.
More precisely, we wish to know how to establish coherence and strictification theorems, that would allow us, when working internally to a model of a weak type theory, to pretend that it satisfies more strict equalities than it actually does, by replacing it by an equivalent stricter model.
The question of the conservativity of strong identity types over weak identity types has been asked at the TYPES 2017 conference~\cite{WeakJTypes}, motivated by the fact that the path types in cubical type theory only satisfy the elimination principle of weak identity types.
This was also the original motivation for the present paper.

We give some examples of weakenings and extensions of homotopy type theory that ought to be equivalent to standard homotopy type theory.
We believe that the coherence theorems presented in this paper brings the proofs of these equivalences within reach.
\begin{exas}\label{exas:applications} \hfill
  \begin{itemize}
    \item Weakening the $\beta$ and $\eta$ computation rules of identity types, $\Sigma$-types, inductive types, etc, gives a weaker variant of HoTT.
    \item We can add strict equalities that make the addition on natural numbers into a strictly associative and commutative operation.
      That is, while the inductive definition of $(- + -) : \Nat \to \Nat \to \Nat$ only satisfies the strict equalities $0 + y = y$ and $(\mathsf{suc}\ x) + y = \mathsf{suc}\ (x + y)$, we would add the strict equalities $x + 0 = 0$, $x + (\mathsf{suc}\ y) = \mathsf{suc}\ (x + y)$, $x + y = y + x$, $(x + y) + z = x + (y + z)$, etc.
    \item Similarly, we could make the composition of equalities into a strictly associative operation, optionally with strict inverses.
    \item We can extend the theory with a universe of strict proposition~\cite{SProp} that is equivalent to the universe of homotopy propositions.
    \item Similarly, we can extend the theory with universes of strict categories, strict rings, etc, that satisfy strictly the equations of the theories of categories, rings, etc.
    \item We can extend the theory with a universe of ``strictly'' pointed types $\mathsf{SPtType}$, equivalent to the universe of pointed types $\mathsf{PtType} \triangleq (A : \UU) \times A$, with a smash product operation $(- \wedge -) : \mathsf{SPtType} \to \mathsf{SPtType} \to \mathsf{SPtType}$ with more strict equalities than the smash product of $\mathsf{PtType}$.
      This would provide an alternative interpretation of Brunerie's rewriting based method to prove that the smash product is a symmetric monoidal product on pointed types~\cite{BrunerieSmash}.
  \end{itemize}
\end{exas}

Some progress has been made by Isaev in \cite{IsaevMorita}.
In that paper, Isaev defines the notion of Morita equivalence between type theories, and gives some characterizations of that notion.
A first conservativity result in the absence of UIP is also proven, showing that type theories with weak or strong unit types are Morita equivalent.

The constructions by Isaev~\cite{IsaevMS} and Kapulkin and Lumsdaine~\cite{HoThTT,HoInvCwA}, of Quillen model or semi-model structures over the categories of models of type theories, are also extremely relevant for our work.
In particular, as remarked in \cite{HoThTT}, Hofmann's conservativity theorem proves exactly that the morphism $\Init_{\mathsf{ITT}} \to \Init_{\mathsf{ETT}}$ between the initial models of intensional type theory and extensional type theory is a trivial fibration of their semi-model structure.
The weak equivalences of the same semi-model structure correspond to a weaker notion of conservativity than trivial fibrations.
Isaev's definition of Morita equivalence relies on that notion of weak equivalence.

This paper builds on top of the aforementioned work.
While Isaev considers the notion of Morita equivalence for arbitrary morphisms between type theories, we restrict our attention to the equational extensions of a weak type theory $\Th_{w}$ to a strong type theory $\Th_{s}$, by a family of equations $\Th_{e}$, which should hold weakly in $\Th_{w}$ and strictly in $\Th_{s}$.
We then establish sufficient conditions for the theories $\Th_{w}$ and $\Th_{s}$ to be Morita equivalent.

The situation can be compared to other well-known coherence theorems, such as Mac Lane's coherence theorem for monoidal categories~\cite{MacLaneCoh}.
They can often be stated in multiple different ways.
For example, here are two related ways to state the coherence theorem for monoidal categories.
\begin{enumerate}
  \item\label{itm:coh_moncat_1} Every (weak) monoidal category is monoidally equivalent to a strict monoidal category.
  \item\label{itm:coh_moncat_2} In a freely generated monoidal category, every diagram made up of associators and unitors commutes.
\end{enumerate}

The statement (\ref{itm:coh_moncat_1}) is generally the one that we want to use: it allows us to work with any weak monoidal category as if it was strict.
The statement (\ref{itm:coh_moncat_2}) is however perhaps easier to prove, because free monoidal categories can be seen as syntactic objects, that are relatively easy to describe explicitly and understand.
See \cite{joyal1991geometry} for a proof of the statement (\ref{itm:coh_moncat_1}) that relies on the statement (\ref{itm:coh_moncat_2}).
In the case of monoidal category, it is actually possible to prove the statement (\ref{itm:coh_moncat_1}) more directly using representation theorems similar to the Yoneda lemma.
This kind of approach does not seem suitable for the coherence theorems that we are interested in.

The main result of this paper is a coherence theorem for type theories that is analogous to the fact the statement (\ref{itm:coh_moncat_1}) can be deduced from the statement (\ref{itm:coh_moncat_2}).
It states that to establish the conservativity of the extension of a weak type theory $\Th_{w}$ to a strong type theory $\Th_{s}$ by a family of equations $\Th_{e}$, it suffices to check, for every cellular model $\CC$ of $\Th_{w}$ (a cellular model is a model that is freely generated by some types and terms), that the higher congruence on $\CC$ freely generated by the equations of $\Th_{e}$ exists and is acyclic.
The acyclicity condition encodes the same idea as the fact that every diagram made up of associators and unitors commutes in a freely generated monoidal category.

The main problem lies in the details of the definition of the notion of higher congruence.
An ordinary congruence over a model $\CC$ consists of equivalence relations on the families of types and terms of $\CC$, that should be preserved by all type-theoretic operations.
Equivalently, a congruence can be seen as an extension of the set-valued model $\CC$ to a model valued in setoids.
A higher congruence over $\CC$ should instead be an extension of $\CC$ to a model valued in weak $\infty$-groupoids, or spaces.

Defining higher congruences requires choosing a model of weak $\infty$-groupoids among many.
Our solution is to base our definition of higher congruences on a reformulation of Brunerie's type-theoretic definition of weak $\infty$-groupoid~\cite[Appendix A]{BrunerieThesis}.
We note that Brunerie $\infty$-groupoids are known to be equivalent to the other models of spaces, thanks to work by Henry~\cite{HenryHoHyp}.
Using other models of weak $\infty$-groupoids, such as simplicial or cubical Kan complexes, could also potentially work, but using a type-theoretic definition seems to make the shapes of different objects involved (models of the base type theories and higher congruences) match up.
Concretely, we will define a new type theory $\Th_{w,2}$ extending the weak type theory $\Th_{w}$, and define a higher congruence over a model $\CC$ of $\Th_{w}$ to be a model $\CD$ of $\Th_{w,2}$ whose underlying model of $\Th_{w}$ is equivalent to $\CC$.
The higher congruences freely generated by some equations then become the initial models of some type theories, which are syntactic objects that can be handled using standard type-theoretic techniques (such as parametricity, logical relations, etc).

We don't give any application of our coherence theorem in this paper, which is instead focused on proving general results that hold for a wide class of type theories.
Another paper is in preparation with proofs of acyclicity for type theories with weak variants of the standard type-theoretic structures (identity types, $\Pi$-types, $\Sigma$-types, inductive types, universe, ...).
We also hope to include some of the other examples of \cref{exas:applications}.
The proof of acyclicity relies on ideas from a paper of Lasson~\cite{Lasson14}, which proves the canonicity of the weak $\infty$-groupoid laws definable in Brunerie's type theory.
In our setting that result can be reinterpreted as a proof of the fact that the higher congruence that is freely generated by the empty family of equations is acyclic.
For higher congruences generated by non-empty families of equations, Lasson's construction can be combined with a normalization proof.
As a general heuristic, we expect acyclicity, and hence coherence and conservativity, to hold whenever the strong type theory admits a well-behaved normalization algorithm.

\subsection*{Outline of the paper}

In \cref{sec:background}, we introduce our notations and conventions, and review the notion of Category with Families (CwFs) and associated definitions (contextual CwFs, cumulative CwFs and families of telescopes).
We make extensive use of the internal language of presheaf categories to describe our constructions.
We also give a formal definition of type theory signature, extending the notion of QIIT-signature of \cite{QIITs}, although we only use it informally in the rest of the paper.

In \cref{sec:weak_id_types}, we define the structures of weak identity types and weak $\Pi$-types, and derive some basic tools that are necessary to work with them.
In particular, we don't assume that our type theories include $\Sigma$-types, but we prove results that allow us to work as if we had $\Sigma$-types.

In \cref{sec:homotopy_theory} we recall, and adapt to our setting, the classes of maps of the semi-model structure introduced in \cite{HoThTT}.
We also study further the trivial fibrations, which are defined by some surjectivity conditions, and show that they correspond (up to (contextual) isomorphism) to the quotients of a well-behaved class of congruences, which we call fibrant congruences.

In \cref{sec:equivalence_models} we define the notion of equational extension of a theory by a family of internal equalities and recall the notion of Morita equivalence between type theories of \cite{IsaevMorita}.

In \cref{sec:conservativity_strict} we use the notion of fibrant congruence and its relationship with trivial fibrations to obtain characterizations of Morita equivalences of type theories for strict type theories, i.e. type theories that satisfy the UIP principle.
We obtain the following variant of Hofmann's conservativity theorem.
\begin{thm*}[Simplified statement of \cref{thm:conservativity_strict}]
  Let $\Th_{w}$ be a type theory with a cumulative hierarchy of universes and weak identity types satisfying the UIP principle.
  Let $\Th_{s}$ be the extension of $\Th_{w}$ with the equality reflection rule.

  If either of the following two conditions is verified, then $\Th_{w}$ and $\Th_{s}$ are Morita equivalent.
  \begin{enumerate}
    \item The theory $\Th_{w}$ includes $\Pi$-types with a strict $\beta$-rule.
    \item The category $\CMod_{w}^{\cxl}$ of contextual models of $\Th_{w}$, equipped with the classes of weak equivalences, fibrations and cofibrations defined in \cref{sec:homotopy_theory}, is a semi-model category.
      \defiEnd
  \end{enumerate}
\end{thm*}

In \cref{sec:conservativity_nonstrict} we introduce our notion of type-theoretic higher congruence, and use it to obtain characterizations of Morita equivalences for equational extensions of type theories.

Given any type theory $\Th_{w}$, we define a type theory $\Th_{w,2}$ extending $\Th_{w}$, such that ordinary models of $\Th_{w,2}$ correspond to models of $\Th_{w}$ valued in $\infty$-groupoids.

Given any equational extension $\Th_{e}$ over $\Th_{w}$, we will define a further extension $\Th_{w,2,e}$ of $\Th_{w,2}$.
The left adjoint $L : \CMod_{w} \to \CMod_{w,2,e}$ of the adjunction between the categories of models of $\Th_{w}$ and $\Th_{w,2,e}$ can be seen as a functor associating to every model $\CC$ of $\Th_{w}$ the higher congruence over $\CC$ freely generated by the equations of $\Th_{e}$.

Using these notions, we prove the following coherence theorem.
\begin{thm*}[Simplified statement of \cref{thm:conservativity_nonstrict}]
  Let $\Th_{w}$ be a type theory with a cumulative hierarchy of universes and weak identity types, and let $\Th_{e}$ be a family of internal equalities of $\Th_{w}$.
 
  Let $\Th_{s}$ be the extension of $\Th_{w}$ obtained by making the internal equalities of $\Th_{e}$ strict.

  If for every cellular (i.e. freely generated) model $\CC$ of $\Th_{w}$, the morphism $\CC \to L_{w,2,e}\ \CC$ is a weak equivalence and $L_{w,2,e}\ \CC$ is acyclic, then $\Th_{w}$ and $\Th_{s}$ are Morita equivalent.
  \defiEnd
\end{thm*}

In \cref{sec:first_weq}, we show that the first condition of \cref{thm:conservativity_nonstrict} holds as soon as the theory $\Th_{w}$ has $\Pi$-types with a strict $\beta$-rule.
We obtain the following coherence theorem, which is the main theorem of this paper.
\begin{thm*}[Simplified statement of \cref{thm:conservativity_nonstrict_pi}]
  Let $\Th_{w}$ be a type theory with a cumulative hierarchy of universes and weak identity types, and let $\Th_{e}$ be a family of internal equalities of $\Th_{w}$.
  We assume that $\Th_{w}$ also includes $\Pi$-types with a strict $\beta$-rule.
  We also assume that $\Th_{e}$ includes the computation rules of the weak identity types of $\Th_{w}$.
  Let $\Th_{s}$ be the extension of $\Th_{w}$ obtained by making the internal equalities of $\Th_{e}$ strict.

  If for every cellular (i.e. freely generated) model $\CC$ of $\Th_{w}$, $L_{w,2,e}\ \CC$ is acyclic, then $\Th_{w}$ and $\Th_{s}$ are Morita equivalent.
  \defiEnd
\end{thm*}

\subsection*{Agda formalization}
Some of our constructions are expressed in the type-theoretic internal language of presheaf categories, and have been formalized in Agda. The Agda development can be found in the files attached to the arXiv version of the paper or at \url{https://rafaelbocquet.gitlab.io/Agda/CoherenceStrict/}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
