\section{Coherence for strict type theories}\label{sec:conservativity_strict}

In this section we specialize the relationship between strong contextual equivalences and fibrant congruences to the setting of equational extensions of theories.
As a byproduct, we obtain a decomposition of Hofmann's proof of the conservativity of extensional type theories over type theories satisfying the UIP principle.
We assume given a weak type theory $\Th_{w}$ and a strong type theory $\Th_{s}$ extending $\Th_{w}$ by a family of equations $\Th_{e}$.

\begin{defi}
  We say that a contextual congruence $\widetilde{\CC}$ over a model $\CC$ of $\Th_{w}$ includes the marked equations of the equational extension $\Th_{e}$ if, for every finite cellular model $\Init_{w}[X]$, marked equation $p : \abs{\Tm_{\Init_{w}[X]}}\ (\Id\ a\ b)$, object $\Gamma : \abs{\CC}$ and morphism $F : \Init_{w}[X] \to (\CC \sslash \Gamma)$, we have $(F\ a \sim F\ b) \in \widetilde{\CC}$ and $(F\ p \sim \refl\ \{F\ a\}) \in \widetilde{\CC}$.
 
  For example, for the extension from weak identity types to strong identity types, this says that $(\J\ P\ d\ \refl \sim d) \in \widetilde{\CC}$ and $(\J_{\beta}\ P\ d \sim \refl\ \{d\}) \in \widetilde{\CC}$ for all relevant arguments.
  \defiEnd
\end{defi}

\begin{lem}\label{lem:congruence_s}
  Let $\CC$ be a contextual model of $\Th_{w}$.
  Assume given a fibrant contextual congruence $\widetilde{\CC}$ over $\CC$, that is compatible with $\Th_{w}$ and includes the marked equations of the equational extension $\Th_{e}$.
  Then the quotient $\Quot_{\widetilde{\CC}}$ is a model of $\Th_{s}$.
\end{lem}
\begin{proof}
  Since the congruence $\widetilde{\CC}$ is fibrant, we can form the quotient $\Quot_{\widetilde{\CC}}$, and we know that the quotienting map $\quot_{\widetilde{\CC}} : \CC \to \Quot_{\widetilde{\CC}}$ is a strong contextual equivalence. Because $\widetilde{\CC}$ is compatible with $\Th_{w}$, the quotient $\Quot_{\widetilde{\CC}}$ is a model of $\Th_{w}$, and $\quot_{\widetilde{\CC}} : \CC \to \Quot_{\widetilde{\CC}}$ is a morphism of models of $\Th_{w}$.

  To show that $\Quot_{\widetilde{\CC}}$ is a model of $\Th_{s}$, it suffices to check that it satisfies all of the necessary equations.

  Take a cellular model $\Init_{w}[X]$ and a marked equation $p : \abs{\Tm_{\Init_{w}[X]}}\ (\Id\ a\ b)$.
  We need to check that for every morphism $F : \Init_{w}[X] \to (\Quot_{\widetilde{\CC}} \sslash \Gamma)$, $F$ maps $p$ to the reflexivity equality.

  Take such a morphism $F$.
  Since $\CC$ is contextual and $\quot_{\widetilde{\CC}}$ is a strong contextual equivalence, $\quot_{\widetilde{\CC}} : \CC \to \Quot_{\widetilde{\CC}}$ is surjective on contexts. Therefore we have some $\Gamma_0 : \abs{\CC}$ such that $\quot_{\widetilde{\CC}}\  \Gamma_{0} = \Gamma$.
  Note that the morphism $\quot_{\widetilde{\CC}} : \CC \to \Quot_{\widetilde{\CC}}$ can be restricted to $\quot_{\widetilde{\CC}} : (\CC \sslash \Gamma_{0}) \to  (\Quot_{\widetilde{\CC}} \sslash \Gamma)$.

  We will now construct a morphism $F_{0} : \Init_{w}[X] \to (\CC \sslash \Gamma_{0})$ such that $F_{0} \cdot \quot_{\widetilde{\CC}} = F$.
  The universal property of $\Init_{w}[X]$ says that $F$ is determined by the images of the generating terms of $X$.
  To construct $F_{0}$, we just have to pick a lift along $\quot_{\widetilde{\CC}}$ of the images of these generating terms.
  This is possible since $\quot_{\widetilde{\CC}}$ is a strong contextual equivalence.

  By hypothesis, $(F_{0}\ a \sim F_{0}\ b) \in \widetilde{\CC}$ and $(F_{0}\ p \sim \refl) \in \widetilde{\CC}$. Therefore, $\quot_{\widetilde{\CC}}\ (F_{0}\ a) = \quot_{\widetilde{\CC}}\ (F_{0}\ b)$ and $\quot_{\widetilde{\CC}}\ (F_{0}\ p) = \refl$, as needed.
 
  Thus all marked equations hold strictly in $\Quot_{\widetilde{\CC}}$, which is therefore a model of $\Th_{s}$.
\end{proof}

\begin{lem}\label{lem:congruence_seq}
  Let $\Init_{w}[X]$ be a cellular model of $\Th_{w}$.
  Assume that there exists a congruence $\widetilde{\Init_{w}[X]}$ over $\Init_{w}[X]$ satisfying the conditions of \cref{lem:congruence_s} and that is additionally included in the kernel $\ker \eta^{X}$, i.e. any types or terms that are congruent in $\widetilde{\Init_{w}[X]}$ are identified by $\eta^{X}$.
  Then the morphism $\eta^{X} : \Init_{w}[X] \to \Init_{s}[X]$ is a strong contextual equivalence.
\end{lem}
\begin{proof}
  The inclusion $\widetilde{\Init_{w}[X]} \subseteq \ker \eta^{X}$ implies, by the universal property of the quotient $\Quot_{\widetilde{\Init_{w}[X]}}$, that $\eta^{X}$ factors through $\quot_{\widetilde{\Init_{w}[X]}}$; we have $r : \Quot_{\widetilde{\Init_{w}[X]}} \to \Init_{s}[X]$ such that $\eta^{X} = \quot_{\widetilde{\Init_{w}[X]}} \cdot r$.
  \Cref{lem:congruence_s} says that $\Quot_{\widetilde{\Init_{w}[X]}}$ is a model of $\Th_{s}$.
  The universality of the arrow $\eta^{X} : \Init_{w}[X] \to \Init_{s}[X]$ then provides a section $s : \Init_{s}[X] \to \Quot_{\widetilde{\Init_{w}[X]}}$ of $r$.
  By the universal property of $\Init_{w}[X]$, we also have that $\eta^{X} \cdot s = \quot_{\widetilde{\Init_{w}[X]}}$.

  \begin{center}\(\begin{tikzcd}
      & \Quot_{\widetilde{\Init_{w}[X]}} \ar[rd, shift right, swap, "r"] & \\
      \Init_{w}[X] \ar[ru, "\quot_{\widetilde{\Init_{w}[X]}}"] \ar[rr, swap, "\eta^{X}"] && \Init_{s}[X] \ar[lu, shift right, swap, "s"]
    \end{tikzcd}\)\end{center}
  We can now see that $\eta^{\CC}$ is a retract of $\quot_{\widetilde{\CC}}$: the following diagram commutes.
  \begin{center}\(\begin{tikzcd}
      \Init_{w}[X] \ar[r, equal] \ar[d, "\eta^{X}"] &
      \Init_{w}[X] \ar[r, equal] \ar[d, "\quot_{\widetilde{\Init_{w}[X]}}"] &
      \Init_{w}[X] \ar[d, "\eta^{X}"] \\
      \Init_{s}[X] \ar[r, "s"] &
      \Quot_{\widetilde{\Init_{w}[X]}} \ar[r, "r"] &
      \Init_{s}[X]
    \end{tikzcd}\)\end{center}
  The left square of that diagram commutes thanks to the universal property of $\Init_{w}[X]$.

  Since $\quot_{\widetilde{\CC}}$ is a strong contextual equivalence and strong contextual equivalences are closed under retracts, $\eta_{\CC}$ is also a strong contextual equivalence.
\end{proof}

\begin{thm}\label{thm:conservativity_strict}
  Let $\Th_{w}$ be a type theory over the theory of cumulative CwFs with universes and weak identity types that includes the UIP principle and let $\Th_{s}$ be the extension of $\Th_{w}$ with the equality reflection rule.

  If either of the following two conditions holds, then the theories $\Th_{w}$ and $\Th_{s}$ are Morita equivalent.
  \begin{enumerate}
    \item The theory $\Th_{w}$ includes $\Pi$-types with a strict $\beta$-rule (and function extensionality).
    \item The theory $\Th_{w}$ is semi-model.
  \end{enumerate}
\end{thm}
\begin{proof}
  We have to show that for every cellular model $\Init_{w}[X]$ of $\Th_{w}$, the morphism $\eta^{X} : \Init_{w}[X] \to \Init_{s}[X]$ is a strong contextual equivalence.
  We will do so using \cref{lem:congruence_seq}.

  We define a congruence $\widetilde{\Init_{w}[X]}$ over $\Init_{w}[X]$.
  \begin{itemize}
    \item Two types $A,B : \abs{\Ty_{w}}_{\Gamma}$ are congruent if there exists some equality $p : \abs{\Tm_{w}}\ (\Id\ \{\UU\}\ A\ B)$.
    \item Two terms $a : \abs{\Tm_{w}}_{\Gamma}\ A$ and $b : \abs{\Tm_{w}}_{\Gamma}\ B$ are congruent if there exists some equality $p : \abs{\Tm_{w}}\ (\Id\ \{\UU\}\ A\ B)$ along with some equality $q : \abs{\Tm_{w}}\ (\Id\ \{B\}\ (p^{\star}\ a)\ b)$.
      Since $\Th_{w}$ includes UIP, the choice of $p$ is irrelevant.
  \end{itemize}
  The reflexivity, symmetry and transitivity properties are easily seen to hold.

  We can also check that $\widetilde{\Init_{w}[X]}$ is fibrant.
  Indeed, take any two congruent types $(A \sim B) \in \widetilde{\Init_{w}[X]}$ and a term $a : \Tm\ A$ of type $A$.
  Then we have an equality $p$ between $A$ and $B$, and we obtain a term $p^{\star}\ a : \Tm\ B$ such that $(a \sim p^{\star}\ a) \in \widetilde{\Init_{w}[X]}$.

  We still have to check the actions of dependent types and terms on the relations as well as the compatibility with the operations of $\Th_{w}$.
  All operations can be dealt with uniformly; we will only look at the $\Id$ and $\Pi$ type formers.

  In the case of the identity type former $\Id$, we have types $A_{1},A_{2} : \abs{\Ty_{w}}_{\Gamma}$, and terms $x_{1},y_{1} : \abs{\Tm_{w}}_{\Gamma}\ A_{1}$ and $x_{2},y_{2} : \abs{\Tm_{w}}_{\Gamma}\ A_{2}$ such that $(A_{1} \sim A_{2})$, $(x_{1} \sim x_{2})$ and $(y_{1} \sim y_{2})$, and we need to prove that $(\Id\ \{A_{1}\}\ x_{1}\ y_{1} \sim \Id\ \{A_{2}\}\ x_{2}\ y_{2})$.

  By definition of the relations of $\widetilde{\Init_{w}[X]}$, we have internal equalities $p : \abs{\Tm_{w}}_{\Gamma}\ (\Id\ A_{1}\ A_{2})$,
  $q_{x} : \abs{\Tm_{w}}_{\Gamma}\ (\Id\ (p^{\star}\ x_{1})\ x_{2})$ and
  $q_{y} : \abs{\Tm_{w}}_{\Gamma}\ (\Id\ (p^{\star}\ y_{1})\ y_{2})$.
  Here we have to use UIP to ensure that $q_{x}$ and $q_{y}$ both lie over the same type equality $p$.

  We can now see $\Id$ as an operation from the $\Sigma$-type $(A : \UU) \times (x : A) \times (y : A)$ to $\UU$.
  We have not assumed that $\Th_{w}$ has $\Sigma$-types, but we can use telescopes and the results of \cref{ssec:lift_tele} instead.
  Then from $p$, $q_{x}$ and $q_{y}$ we obtain an equality between $(A_{1}, x_{1}, y_{1})$ and $(A_{2}, x_{2}, y_{2})$ in the $\Sigma$-type (or telescope) $(A : \UU) \times (x : A) \times (y : A)$.
  The action on equalities of $\Id$ then provides an equality between $\Id\ \{A_{1}\}\ x_{1}\ y_{1}$ and $\Id\ \{A_{2}\}\ x_{2}\ y_{2}$, as needed.

  Let's also look at an operation with a higher-order argument: the $\Pi$-type former.
  In that case, we have types $A_{1}, A_{2} : \abs{\Ty_{w}}_{\Gamma}$ and dependent types $B_{1} : \abs{\Ty_{w}}_{\Gamma, (x : A_{1})}$ and $B_{2} : \abs{\Ty_{w}}_{\Gamma, (x : A_{2})}$, such that $(A_{1} \sim A_{2})$ and $(B_{1}\ a_{1} \sim B_{2}\ a_{2})$ for every pair $(a_{1} \sim a_{2})$.
  This means that we can find a type equality $p : \Id\ A_{1}\ A_{2}$ and a dependent type equality $q : (a : A_{1}) \to \Id\ (B_{1}\ a)\ (B_{2}\ (p^{\star}\ a))$.
  We need to construct a type equality between $\Pi\ A_{1}\ B_{1}$ and $\Pi\ A_{2}\ B_{2}$.

  There are then two cases.
  \begin{enumerate}
    \item If $\Th_{w}$ has $\Pi$-types with a strict $\beta$-rule, then we can view the $\Pi$-type former as an operation from the type $(A : \UU) \times (B : A \to \UU)$ to $\UU$.
      We can then conclude as in the case of identity types above.
    \item If the theory $\Th_{w}$ is semi-model, then we view the operation $\Pi$ as the type $\Pi\ \bm{A}\ \bm{B}$ of the cellular model
      \[ \CC \triangleq \Init_{w}[ \bm{A} : \UU, (a : A) \vdash \bm{B}(a) : \UU]. \]
     
      We then consider the cellular model
      \[ \CD \triangleq \Init_{w}\lbrack
      \begin{array}[t]{l} \bm{A_{1}} : \UU, \bm{A_{2}} : \UU, \bm{p} : \Id\ \bm{A_{1}}\ \bm{A_{2}}, \\
        (a : \bm{A_{1}}) \vdash \bm{B_{1}}(a) : \UU, \\
        (a : \bm{A_{2}}) \vdash \bm{B_{2}}(a) : \UU, \\
        (a : \bm{A_{1}}) \vdash \bm{q}(a) : \Id\ \bm{B_{1}}(a)\ \bm{B_{2}}(\bm{p}^{\star}\ a)\ \rbrack.
      \end{array} \]
     
      Our assumptions imply that there is a morphism $F : \CD \to (\Init_{w}[X] \sslash \Gamma)$ that sends $\bm{A_{1}}$ to $A_{1}$, $\bm{A_2}$ to $A_2$, etc.

      Note that we also have two maps $i_{1}, i_{2} : \CC \to \CD$, sending $(\bm{A}, \bm{B})$ respectively to $(\bm{A_{1}}, \bm{B_{1}})$ and $(\bm{A_{2}}, \bm{B_{2}})$.
      The map $i_{1}$ is a composition of two basic $J$-cellular extensions. The first of these two extensions adds $\bm{A_{2}}$ and $\bm{p}$ while the second adds $\bm{B_{2}}$ and $\bm{q}$.
      Since $\Th_{w}$ is semi-model and $\CC$ is contextual and cofibrant, the map $i_{1}$ is a weak equivalence.

      The maps $i_{1}$ and $i_{2}$ also admit a common retraction $r : \CD \to \CC$, which sends $\bm{A_{1}}$ and $\bm{A_{2}}$ to $\bm{A}$, $\bm{B_{1}}$ and $\bm{B_{2}}$ to $\bm{B}$, $\bm{p}$ to the reflexivity equality, and $\bm{q}$ to some proof of $\Id\ \bm{B}(a)\ \bm{B}(\refl^{\star}\ a)$. By $2$-out-of-$3$, the map $r : \CD \to \CC$ is also a weak contextual equivalence.

      Therefore we can lift the reflexivity equality $\refl : \Id\ (\Pi\ \bm{A}\ \bm{B})\ (\Pi\ \bm{A}\ \bm{B})$ from $\CC$ to $\CD$ in order to obtain an equality $\alpha : \Id\ (\Pi\ \bm{A_{1}}\ \bm{B_{1}})\ (\Pi\ \bm{A_{2}}\ \bm{B_{2}})$ in $\CD$.

      Applying the morphism $F : \CD \to (\Init_{w}[X] \sslash \Gamma)$, we obtain an equality between $\Pi\ A_{1}\ B_{1}$ and $\Pi\ A_{2}\ B_{2}$, as needed.

      The reader familiar with the theory of model categories will have noticed that this proof uses the fact that $\CD$ is a cylinder object for $\CC$.
      This method generalizes to arbitrary type-theoretic operations, replacing $\CC$ by a cellular model encoding the premises of the operation and $\CD$ by a suitable cylinder object for $\CC$.
  \end{enumerate}

  We can now conclude the proof.
  The congruence $\widetilde{\Init_{w}[X]}$ is included in $\ker \eta^{X}$, essentially by definition.
  It satisfies all of the conditions of \cref{lem:congruence_s} and \cref{lem:congruence_seq}, and $\eta^{X} : \Init_{w}[X] \to \Init_{s}[X]$ is thus a strong contextual equivalence.
  As this holds for all cellular models of $\Th_{w}$, the theories $\Th_{w}$ and $\Th_{s}$ are Morita equivalent
\end{proof}

\begin{rem}
  \Cref{thm:conservativity_strict} is actually not a generalization of Hofmann's conservativity theorem.
  Indeed, the type theories considered by Hofmann did not include a hierarchy of universes.
  The presence of universes makes the proof simpler and more uniform, since we can use the same relations on types and terms: internal equality.

  In the absence of universes, we have to use another equivalence relation on types.
  One solution is to use local universes~\cite{LocalUniverses}.
  A local universe in a CwF $\CC$ is a pair $(V, E)$ where $V$ is a closed type of $\CC$ and $E$ is a dependent type over $V$.
  A type $A : \abs{\Ty_{\CC}}_{\Gamma}$ is classified by a local universe $(V, E)$ if there is a term $\chi : \abs{\Tm_{\CC}}_{\Gamma}\ V$ such that $A = E[\chi]$.
  For many type theories, including the type theories considered by Hofmann, it is possible to show (for the cellular models) that every type $A$ is classified by some local universe $(V_{A}, E_{A})$, as witnessed by a term $\chi_{A}$.
  For example, in presence of $\Pi$-types and $\Sigma$-types, the type $\Pi\ (x : \Nat)\ (\Id\ x\ x)$ is classified by the local universe $(V, E)$, with
  \begin{alignat*}{2}
    & V && \triangleq (\Nat \to \Nat) \times (\Nat \to \Nat) \\
    & E(f , g) && \triangleq \Pi\ (x : \Nat)\ (\Id\ (f\ x)\ (g\ x)).
  \end{alignat*}
  We can then define a suitable congruence by saying that two types $A, B$ are related if they have the same local universe $(V_{A}, E_{A}) = (V_{B}, E_{B})$, and their classifying terms $\chi_{A}$ and $\chi_{B}$ are internally equal in $V_{A}$.
\end{rem}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
