\section{Background}\label{sec:background}

We recall in this section the semantics of type theories in categories with families (CwFs), and introduce the tools and notations that we will use in this paper.
We will make use in particular of the internal type-theoretic language of the presheaf category $\CPsh\ \CC$ as a tool to define and work with type-theoretic structures over a CwF $\CC$.

\subsection{Metatheory and basic notations}
\begin{enumerate}
  \item We assume that a sufficiently large hierarchy of universes is available in the ambient metatheory.
    The sets of dependent function are written $(a : A) \to B(a)$, and dependent functions are introduced by $(a : A) \mapsto b(a)$.
    We often use braces $\{\}$ to indicate implicit arguments.
    For instance, given a function $f : \{a : A\} \to B\ a \to C\ a$, and elements $a : A$ and $b : B\ a$, we will just write $f\ b$ for the application of $f$ to $a$ and $b$.
    We write $f\ \{a\}\ b$ or $f_{a}\ b$ when we want to make the argument $a$ explicit.

    The sets of dependent pairs are written $(a : A) \times B(a)$, and dependent pairs are introduced by $(a,b)$.
  \item We assume the axiom of choice.
    As currently formulated, our results do not hold constructively, as we will note in \cref{rem:split_equiv}.
    We believe that they could be reformulated so as to hold constructively.
    Alternatively, it should be possible to bypass the non-constructive parts in the case of type theories with decidable equality and decidable type checking.
  \item We denote the set of objects of a small category $\CC$ by $\abs{\CC}$, and the set of morphisms between $x,y : \abs{\CC}$ by $\CC(x \to y)$.
    We may also quantify over objects of a category using either $(x : \CC)$ or $(x : \CC^{\op})$ instead of $(x : \abs{\CC})$; in that case, it is often understood that all constructions depending on $x$ are covariantly or contravariantly natural (or functorial) in $x$.
  \item We denote the composition of morphisms $f : \CC(A \to B)$ and $g : \CC(B \to C)$ by either $(f \cdot g) : \CC(A \to C)$ or $(g \circ f) : \CC(A \to C)$.
    The diagrammatic composition order $(f \cdot g)$ is preferred in presence of commutative diagrams, contravariant actions on the left (e.g. $f^{\star}\ (g^{\star}\ X) = (f \cdot g)^{\star}\ X$) and covariant actions on the right.
    The standard composition order $(g \circ f)$ is usually used in presence of contravariant actions on the right (e.g. $X[f][g] = X[f \circ g]$) and covariant actions on the left (e.g. $(f \circ g)(x) = f(g(x))$).
  \item Given a category $\CC$, we denote the slice category over an object $x : \abs{\CC}$ by $(\CC / X)$, and the coslice category under an object $x : \abs{\CC}$ by $(C \backslash x)$.
\end{enumerate}

\subsection{Internal language of presheaf categories}
Fix a base category $\CC$.
We recall how the presheaf category $\CPsh\ \CC$ is given the structure of a model of extensional type theory.
We refer the reader to \cite{SyntaxAndSemantics,LiftingUniverses} for a more detailed presentation of this structure.

\begin{enumerate}
  \item The objects of $\CPsh\ \CC$ are presheaves over $\CC$, i.e. functors from $\CC^{\op}$ to $\CSet$, and the morphisms are natural transformations.
    Given a presheaf $X : \CPsh\ \CC$, we denote by $\abs{X}_{\Gamma} : \SSet$ its component at an object $\Gamma : \abs{\CC}$, and write $x[f] : \abs{X}_{\Gamma}$ for the restriction of an element $x : \abs{X}_{\Delta}$ by a morphism $f : \CC(\Gamma \to \Delta)$.
    We may also occasionally write $\abs{X}_{f} : \abs{X}_{\Delta} \to \abs{X}_{\Gamma}$ for the restriction operation.
  \item A type of $\CPsh\ \CC$ over a presheaf $X : \CC$ is a dependent presheaf over $X$, or equivalently a presheaf over the category of elements $(\CC/X)$.
    Given a dependent presheaf $Y$ over $X$, we may denote its component at an object $\Gamma : \abs{\CC}$ by $\abs{Y}_{\Gamma} : (x : \abs{X}_{\Gamma}) \to \SSet$.
    We may also introduce a dependent presheaf $Y$ over $X$ by writing
    \[ Y : \{\Gamma : \CC^{\op}\} \to \abs{X}_{\Gamma} \to \SSet. \]
    A term of type $Y$ over a presheaf $X : \CC$ is a dependent natural transformation from $X$ to $Y$, or equivalently a global element of $Y$ when seen as a presheaf over $(\CC/X)$.
    We may introduce a dependent natural transformation $y$ from $X$ to $Y$ by writing
    \[ y : \{\Gamma : \CC^{\op}\} \to (x : \abs{X}_{\Gamma}) \to \abs{Y}_{\Gamma}\ x. \]
  \item The Yoneda embedding is written $\yo : \CC \to \CPsh\ \CC$.
    The presheaf represented by an object $\Gamma : \abs{\CC}$ is written $\yo_{\Gamma} : \CPsh\ \CC$.
  \item The presheaf universe $\SPsh_{\CC,i}$ is the classifier of $i$-small dependent presheaves.
    Given any presheaf $X$, a global element of $\SPsh_{\CC,i}$ over $X$ is a dependent presheaf over $X$.
    Its definition can be computed using the Yoneda lemma: for every object $\Gamma : \CC$, $\abs{\SPsh_{\CC,i}}_{\Gamma}$ has to be (isomorphic to) the set of $i$-small dependent presheaves over the representable presheaf $\yo_{\Gamma}$.

    We will ignore most size issues and omit universe levels in this paper, and write just $\SPsh_{\CC}$ for the presheaf universe at any universe level.
  \item Most type structures available externally, such as $\Pi$-types, $\Sigma$-types, quotients and indexed inductive types are also available in the presheaf model $\CPsh\ \CC$.
    The presheaf universes $\SPsh_{\CC}$ are closed under those.
    We use the same notations internally to $\CPsh\ \CC$ as in the external metatheory, e.g. $(a : A) \to B\ a$ for $\Pi$-types, etc.

    Even though we assume the axiom of choice in our external metatheory, it may not hold internally to $\CPsh\ \CC$.
  \item The presheaf model $\CPsh\ \CC$ supports extensional equality types; we can reason about equality internally in the same way as we do externally.
\end{enumerate}
We will also need the notion of locally representable dependent presheaf, which is the semantic counterpart of the syntactic notion of context extension.
\begin{defi}
  A dependent presheaf $Y : \CPsh\ (\CC/X)$ is said to be \defemph{locally representable} if for every $\Gamma : \abs{\CC}$ and $x : \abs{X}_{\Gamma}$, the presheaf $Y_{x} : \CPsh\ (\CC/\Gamma)$ defined by \[ (\CC/\Gamma)^{\op} \ni (\rho : \CC(\Delta \to \Gamma)) \mapsto \abs{Y}_{\Delta}\ x[\rho] \in \CSet \] is representable.

  This condition can be unfolded into one of the following equivalent definitions.
  \begin{enumerate}
    \item For every $\Gamma : \abs{\CC}$ and $x : \abs{X}_{\Gamma}$, we have an extended object $\Gamma \rhd x$, a projection map $\mathbf{p}_{x} : \CC(\Gamma \rhd x \to \Gamma)$ and a generic element $\mathbf{q}_{x} : \abs{Y}_{\Gamma \rhd x}\ x[\mathbf{p}_{x}]$, satisfying a universal property: for every $\Delta : \abs{\CC}$, $\rho : \CC(\Delta \to \Gamma)$ and $y : \abs{Y}_{\Delta}\ x[\rho]$, there is a unique map $\langle \rho, y \rangle : \CC(\Delta \to (\Gamma \rhd x))$ such that $\langle \rho, y \rangle \cdot \mathbf{p}_{x} = \rho$ and $\mathbf{q}_{x}[\langle \rho,y \rangle] = y$.

      The reader familiar with the notion of category with families will have recognized the combinators used in Dybjer's original definition of CwF~\cite{InternalTT}.
    \item For every $\Gamma : \abs{\CC}$ and $x : \abs{X}_{\Gamma}$, the category of elements $(\CC/\Gamma/Y_{x})$ has a terminal object $(\Gamma \rhd x, \mathbf{p}_{x}, \mathbf{q}_{x})$.
    \item The natural transformation $\pi_{1} : \Sigma\ X\ Y \to X$ is a representable natural transformation in $\CPsh\ \CC$, i.e. for every representable presheaf $\yo\ \Gamma$ of $\CC$ and natural transformation $x : \yo\ \Gamma \to X$, there is a pullback square
      \[ \begin{tikzcd}
          \yo\ (\Gamma \rhd x) \ar[r] \ar[d] \ar[rd, phantom, very near start, "\lrcorner"] & \Sigma\ X\ Y \ar[d, "\pi_{1}"] \\
          \yo\ \Gamma \ar[r, "x"] & X
        \end{tikzcd} \]
      where the pullback $\yo\ (\Gamma \rhd x)$ is representable.
      See also Awodey's definition of natural model of type theory~\cite{NaturalModels}.
    \item We work in the internal language of $\CPsh\ \CC$ and write $\Ob_{\CC}$ for the constant presheaf of objects of $\Ob_{\CC}$ and $\yo : \Ob_{\CC} \to \SPsh_{\CC}$ for the internalization of the Yoneda embedding. We have global elements $X : \SPsh_{\CC}$ and $Y : X \to \SPsh_{\CC}$.

      For every global object $\Gamma : \Ob_{\CC}$ and global element $x : \yo\ \Gamma \to X$, the following type is inhabited
      \[ ((\Gamma \rhd x) : \Ob_{\CC}) \times (\yo\ (\Gamma \rhd x) \simeq ((\gamma : \yo\ \Gamma) \times Y\ (x\ \gamma))). \]
      The notion of \emph{global} element could be internalized using the flat modality of crisp type theory, as done in \cite{LicataOPS18}.
  \end{enumerate}
  Local representability is a structure on dependent presheaves, although it is categorically irrelevant.
  There are universes $\SRepPsh_{\CC,i}$ of $i$-small locally representable presheaf families, defined analogously to the presheaf universes.
  The universes $(\SRepPsh_{\CC,i})$ of representable presheaf families are closed under dependent pairs.
  \defiEnd
\end{defi}
From now on, we will say that a dependent presheaf is representable to mean that it is locally representable.
Because we usually consider dependent presheaves, for which there is no non-local notion of representability, this should be unambiguous.
Also note that in presence of finite products, local representability is equivalent to representability for non-dependent presheaves.

\subsection{Categories with (representable) families}

We use \defemph{categories with families} (CwFs) as our models of type theory.

\begin{defi}
  Internally to a presheaf model $\CPsh\ \CC$, an \defemph{internal representable family} is a pair $(\Ty,\Tm)$ with $\Ty : \SPsh_{\CC}$ and $\Tm : \Ty \to \SRepPsh_{\CC}$.

  A \defemph{category with families} (CwF) is a category $\CC$ equipped with a distinguished terminal object (written $\diamond$) and a global representable family $(\Ty_{\CC},\Tm_{\CC})$, consisting of a presheaf $\Ty_{\CC}$ and a locally representable dependent presheaf $\Tm_{\CC}$ over $\Ty_{\CC}$.
  \defiEnd
\end{defi}

One can check that unfolding this definition gives a notion of CwF that is isomorphic (or at least equivalent, depending on the precise definitions of the universes $\SPsh_{\CC}$ and $\SRepPsh_{\CC}$) to the standard definition.

A presentation of a type theory consists of algebraic operations and equations over CwFs.
This definition can be made formal using a notion of type theory signature extending the notion of QIIT-signature from \cite{QIITs}.
\begin{defi}\label{def:th_sig}
  The type theory of type theory signatures is defined to include the following structures.
  \begin{enumerate}
    \item A universe $\UU$ of sorts.
      \begin{mathpar}
        \inferrule{ }{\UU\ \type}

        \inferrule{A : \UU}{\El\ A\ \type}
      \end{mathpar}
    \item A subuniverse $\overline{\UU}$ of representable sorts (sorts for which context extension is permitted).
      \begin{mathpar}
        \inferrule{ }{\overline{\UU}\ \type}

        \inferrule{A : \overline{\UU}}{A : \UU}
      \end{mathpar}
    \item Dependent function types with arities in $\UU$.
      \begin{mathpar}
        \inferrule{A : \UU \\ [a : \El\ A]\ B(a)\ \type}{\Pi\ A\ B\ \type}

        \inferrule{[a : \El\ A]\ b(a) : B(a)}{\lam\ b : \Pi\ A\ B}

        \inferrule{f : \Pi\ A\ B \\ a : \El\ A}{\app\ f\ a : B(a)}
      \end{mathpar}
      They are used to specify the arguments of the operations and equations in a signature.
      The facts that these $\Pi$-types take arities in $\UU$ is similar to the strict positivity restriction of inductive types.
    \item Dependent function types in $\UU$ with arities in $\overline{\UU}$.
      \begin{mathpar}
        \inferrule{A : \overline{\UU} \\ [a : \El\ A]\ B(a) : \UU}{\overline{\Pi}\ A\ B : \UU}

        \inferrule{[a : \El\ A]\ b(a) : \El\ B(a)}{\overline{\lam}\ b : \El\ (\overline{\Pi}\ A\ B)}

        \inferrule{f : \El\ (\overline{\Pi}\ A\ B) \\ a : \El\ A}{\overline{\app}\ f\ a : \El\ B(a)}
      \end{mathpar}
      They are used to encode the fact that the arguments of the operations and equations in a signature can live in extended contexts.
    \item Extensional equality types.
      \begin{mathpar}
        \inferrule{A\ \type \\ x,y : A}{\Eq_{A}\ x\ y\ \type}

        \inferrule{x : A}{\refl\ x : \Eq_{A}\ x\ x}

        \inferrule{p : \Eq_{A}\ x\ y}{x = y}

        \inferrule{p : \Eq_{A}\ x\ x}{p = \refl\ x}
      \end{mathpar}
    \item A unit type and dependent pair types.
      \begin{mathpar}
        \inferrule{ }{\Unit\ \type}

        \inferrule{A\ \type \\ [a : A]\ B(a)\ \type}{\Sigma\ A\ B\ \type}
      \end{mathpar}
  \end{enumerate}
  A \defemph{type theory signature} is a closed type in the syntax of the theory of type theory signatures.
  An \defemph{extension} of a type theory signature $\Th$ is a dependent type $\Th'$ over $\Th$.
  It gives rise to an extended signature $(x : \Th) \times \Th'\ x$.
  \defiEnd
\end{defi}

Any type theory signature can be interpreted in any presheaf model $\CPsh_{\CC}$; the universe $\UU$ of sorts is interpreted as the presheaf universe $\SPsh_{\CC}$, the universe $\overline{\UU}$ of representable sorts is interpreted as the universe $\SRepPsh_{\CC}$ of locally representable presheaf families, and the other components are interpreted by the standard $\Pi$-types, $\Sigma$-types, and equality types of the presheaf model.
For example, the signature of CwFs is \[ \Th_{\mathsf{CwF}} \triangleq (\Ty : \UU) \times (\Tm : \Ty \to \overline{\UU}). \]
The interpretation of $\Th_{\mathsf{CwF}}$ in a presheaf model gives exactly the presheaf of internal representable families.
The sort $\Tm$ of terms is representable, while the sort $\Ty$ of types is not.
Thus type-theoretic structures extending the signature of CwFs are allowed to contain operations with higher-order arguments (i.e. binders), but the higher-order arguments can themselves only depend on $\Tm$.

Other type-theoretic structures can be described by extensions of the signature of CwFs.
For example, the structure of $\Pi$-types with strict $\beta$ and $\eta$ rules consists of operations $\Pi$, $\lam$ and $\app$ and equations $\app_{\beta}$ and $\Pi_{\eta}$, specified by the following signature extending $\Th_{\mathsf{CwF}}$.
\begin{alignat*}{2}
  & \Pi && : (A : \Ty) (B : \overline{\Tm\ A} \to \Ty) \to \Ty \\
  & \lam && : \{A,B\} (b : \overline{(a : \Tm\ A)} \to \Tm\ (B\ a)) \to \Tm\ (\Pi\ A\ B) \\
  & \app && : \{A,B\} (f : \Tm\ (\Pi\ A\ B)) (a : \Tm\ A) \to \Tm\ (B\ a) \\
  & \app_{\beta} && : \{A,B,b,a\} \to \app\ (\lam\ b)\ a = (b\ a) \\
  & \Pi_{\eta} && : \{A,B,f\} \to \lam\ (\app\ f) = f
\end{alignat*}

Again, one can check that unfolding the interpretation of this signature in presheaf categories gives a definition that is equivalent to the usual external definition of $\Pi$-type structures over CwFs.

Note that this definition of type-theoretic structure allows us to work with the syntax of type theories in the internal language of presheaf categories using higher-order abstract syntax (HOAS).
Indeed, presheaf models have been used to justify HOAS~\cite{SyntaxAndSemantics,HofmannHOAS}.

We included the definition of type theory signature for completeness, and to justify the quantifications on all type-theories appearing in this paper.
However, being fully formal with it would require us to develop its theory further, which we believe to be outside of the scope of this paper.
Thus we will only use this notion informally.
It may seem to invalidate our claim that our theorems are valid for arbitrary type theories.
However, it will be quite clear that all of the constructions that we perform in this paper and that depend on the actual signature are uniform in the type-theoretic operations of the signature.
For instance, when defining the interpretation of the $\Pi$ type former in a model, the interpretation will only depend on the shape of the type former $\Pi : (A : \Ty) (B : \overline{\Tm\ A} \to \Ty) \to \Ty$, but never on the presence of any other operation in the signature, and would work just as well for any other type former $X : (A : \Ty) (B : \overline{\Tm\ A} \to \Ty) \to \Ty$.

All of the arguments presented in this paper can alternatively be checked independently for any concrete type theory.

We however refer the reader to two similar general definitions of type theories.
Capriotti's rule framework~\cite{CappriottiRF,CapriottiThesis} is similar to our definition, without the strict positivity restriction on $\Pi$-types, and without the $\Pi$-types with arities in $\overline{\UU}$.
This implies that the type formers can include unrestricted higher-order arguments, which may fail to have well-defined categories of models or initial models.
Uemura's representable map categories~\cite{GeneralFrameworkTT} can encode almost the same type theories as our definition.
The main difference is that our definition generalizes QIIT-signatures and generalized algebraic theories (i.e. algebraic theories with dependent sorts), whereas Uemura's definition generalizes essentially algebraic theories (i.e. algebraic theories with partial operations).
This does not change the class of presentable type theories, but the additional structure of generalized algebraic theories is crucial for the present paper.
Another minor difference is that we only consider finite signatures.
The semantics of representable map categories are given by functorial semantics, whereas the semantics for our notion of signature is more directly defined by induction on the signatures, as in~\cite{QIITs}.

We will only consider type theories that extend the signature of CwFs (and cumulative CwFs, which will be introduced later) by new operations and equations only, that is type theories whose only sorts are the non-representable sort of types and the representable sort of terms.

From the generalized algebraic presentations of CwFs and type-theoretic structures, we obtain a $1$-category $\CMod_{\Th}$ of models from any signature $\Th$.
The objects of $\CMod_{\Th}$ are CwFs equipped with the additional type-theoretic structures of $\Th$.
The morphisms are functors, with additional actions on types and terms, strictly preserving the chosen terminal object and the representing objects for the context extensions and the type-theoretic operations of $\Th$.
Given a morphism $F : \CMod_{\Th}(\CC \to \CD)$, we will denote its actions on types and terms by $F : \{\Gamma : \CC^{\op}\} \to \abs{\Ty_{\CC}}_{\Gamma} \to \abs{\Ty_{\CD}}_{F\ \Gamma}$ and $F : \{\Gamma : \CC^{\op}\}\{A : \abs{\Ty_{\CC}}_{\Gamma}\} \to \abs{\Tm_{\CC}}_{\Gamma}\ A \to \abs{\Tm_{\CD}}_{F\ \Gamma}\ (F\ A)$.

We also automatically obtain the existence of an initial object $\Init_{\Th}$ of $\CMod_{\Th}$.
We adopt the algebraic point of view on the syntax of type theory: we only work with the abstract characterization of the syntax as the components of an initial object, and don't try to give any more explicit construction of this initial object.

More generally, $\CMod_{\Th}$ is a finitely locally presentable category, and is in particular complete and cocomplete.
We also automatically obtain that freely generated models exist.
We write $\mathsf{Free}(\dots)$ for freely generated models.
For example, $\mathsf{Free}(\bm{\Gamma} \vdash )$ is the model freely generated by a single object $\bm{\Gamma}$, $\mathsf{Free}(\bm{\Gamma} \vdash \bm{A} : \Ty)$ is the model freely generated by an object $\bm{\Gamma}$ and a type $\bm{A}$ over $\bm{\Gamma}$, and $\mathsf{Free}(\bm{\Gamma} \vdash \bm{a} : \Tm\ \bm{A})$ is the model freely generated by an object $\bm{A}$, a type $\bm{A}$ over $\bm{\Gamma}$ and a term $\bm{a}$ of type $\bm{A}$.
We use bold symbols ($\bm{\Gamma}$, $\bm{A}$, $\bm{a}$, etc) to distinguish the generators of a freely generated model.
These models satisfy some universal properties.
For instance, the morphisms $\mathsf{Free}(\bm{\Gamma} \vdash ) \to \CC$ are in natural bijection with the objects of $\CC$.

We denote the $1$-category of CwFs without any additional structure by $\CCwf$.

For some purposes, it may have been preferable or more elegant to work with $2$-categories of models, weak morphisms (i.e. morphisms that only preserve the terminal object and the context extensions up to isomorphism) and natural transformations.
We will however need to consider additional structures on the categories of models that are better developed in the $1$-categorical setting, such as (both orthogonal and weak) factorization systems and semi-model structures.

\subsection{Contextual models}\label{ssec:contextual_models}

An important class of CwFs are the \emph{contextual} CwFs, whose objects and morphisms are really given by lists of types and terms.
Indeed, from some point of view, in the language of type theory, we never explicitly talk about the objects and morphisms of a model, but only about types and terms that live in the same contextual slice of a given model.
Thus only the contextual models matter.
However, a direct definition of contextual models is complicated (their generalized algebraic presentation is infinite), and many intermediate constructions go through non-contextual models.
It is thus more convenient to define contextuality as a property of general models.
Fortunately, they can nicely be described by the means of an orthogonal factorization system on $\CCwf$\footnote{The author learnt of this definition of contextuality from Christian Sattler.}.

We first recall the definition of orthogonal factorization systems, originally introduced in \cite{FreydKellyOFS}.
\begin{defi}
  An \defemph{orthogonal factorization system} on a category $\CC$ consists of two classes of maps $\CL$ and $\CR$ satisfying the following two properties:
  \begin{itemize}
    \item Every map $f : \CC(X \to Y)$ can be factored as $f = l \cdot r$, where $l \in \CL$ and $r \in \CR$.
    \item Every map in $\CL$ is left orthgonal to every map in $\CR$; this means that for every $l \in \CL$ and $r \in \CR$ and commutative square
      \[ \begin{tikzcd}
          A \ar[d, "l"] \ar[r, "f"] & X \ar[d, "r"] \\
          B \ar[r, "g"] & Y \rlap{\, }
        \end{tikzcd} \]
      there exist an unique map $j : \CC(B \to X)$ such that $j \cdot r = g$ and $l \cdot j = f$.
      \defiEnd
  \end{itemize}
\end{defi}

\begin{defi}
  A morphism $F : \CCwf(\CC \to \CD)$ is said to be a \defemph{contextual isomorphism} if its actions on types and terms are bijective.
  \defiEnd
\end{defi}

Let $I$ be the set of maps of $\CCwf$ consisting of $I^{\Ty} : \mathsf{Free}(\bm{\Gamma} \vdash) \to \mathsf{Free}(\bm{\Gamma} \vdash \bm{A} : \Ty)$ and $I^{\Tm} : \mathsf{Free}(\bm{\Gamma} \vdash \bm{A} : \Ty) \to \mathsf{Free}(\bm{\Gamma} \vdash \bm{a} : \Tm\ \bm{A})$.
Contextual isomorphisms are exactly the maps that are right orthogonal to $I$.
The maps that are left orthogonal to the contextual isomorphisms are called \defemph{contextual extensions}.

By the small object argument for orthogonal factorization systems~\cite{Kelly1980}, contextual extensions and contextual isomorphisms form an orthogonal factorization system.
Any morphism $F : \CCwf(\CC \to \CD)$ admits an unique (up to isomorphism) factorization $\CC \to \cxlim F \to \CD$ where $\CC \to \cxlim F$ is a contextual extension and $\cxlim F \to \CD$ is a contextual isomorphism.
The CwF $\cxlim F$ is called the \defemph{contextual image} of $F$.

In particular, given any $\CC : \CCwf$, the unique morphism $\Init \to \CC$ admits such a factorization.
Its contextual image is called the \defemph{contextual core} of $\CC$, and is denoted by $\cxl \CC$.
The map $\cxl \CC \to \CC$ is a contextual isomorphism by definition.
When the map $\cxl \CC \to \CC$ is also an isomorphism of CwFs, we say that $\CC$ is \defemph{contextual}.

This definition of contextuality is equivalent to the usual definition, as found for instance in \cite{CwFUSD}.
\begin{prop}
  A CwF $\CC$ is contextual if and only there exists a length function $l : \abs{\CC} \to \Nat$ such than for any $\Gamma : \abs{\CC}$, if $(l\ \Gamma) = 0$ then $\Gamma = \diamond$ and if $(l\ \Gamma) = n+1$, then there are unique $\Gamma' : \abs{\CC}$ and $A : \abs{\Ty}_{\Gamma'}$ such that $\Gamma = \Gamma' \rhd A$.
  \qed
\end{prop}
% \begin{proof}
%   Generate a relation $P : \Nat \to \abs{\CC} \to \SSet$ inductively by
%   \begin{alignat*}{2}
%     & - && : P\ 0\ \diamond, \\
%     & - && : \{n,\Gamma,A\} \to P\ n\ \Gamma \to P\ (n + A)\ (\Gamma \rhd A).
%   \end{alignat*}
%   We then define a CwF $\CC'$ whose objects are $\abs{\CC'} = (n : \Nat) \times (\Gamma : \abs{\CC}) \times P\ n\ \Gamma$, and whose morphisms, types and terms are induced from $\CC$. \\
%   We have a contextual isomorphism $\CC' \to \CC$. Furthermore, $\CC'$ is contextual: given a lifting problem
%   \[ \begin{tikzcd}
%       \Init \ar[d] \ar[r] & \CA \ar[d] \\
%       \CC' \ar[r] & \CB,
%     \end{tikzcd} \]
%   we can define a solution $\CC' \to \CA$ by induction on the relation $P$. \\
%   Finally, we can easily check that $\CC' \to \CC$ has
% \end{proof}
% \begin{proof}
%   We can equip the set $\Nat$ of natural numbers with the structure of a CwF by defining the families of morphisms, types and terms to be the unit families, and by defining the extension of an object $n$ by the unique type in context $n$ to be $n+1$. In fact, $\Nat$ is then the terminal contextual CwF.
%   The unique CwF morphism $\Nat \to \Unit$ is clearly a contextual isomorphism, and the usual definition of contextuality of a CwF $\CC$ exactly says that exists a CwF morphism $l : \CC \to \Nat$. \\
%   Thus, assuming that a CwF $\CC$ is contextual, the following lifting problem admits a unique solution, which provides a length function witnessing the contextuality of $\CC$.
%   \[ \begin{tikzcd}
%       \Init \ar[r] \ar[d] & \Nat \ar[d] \\
%       \CC \ar[ru, dashed, "l"] \ar[r] & \Unit
%     \end{tikzcd} \]
%   Conversely, assuming that we have a length function $l : \CC \to \Nat$, then for any contextual isomorphism $f : \CA \to \CB$, we can find a solution to a lifting problem of the form
%    \[ \begin{tikzcd}
%        \Init \ar[r] \ar[d] & \CA \ar[d, "f"] \\
%        \CC \ar[r, "h"] & \CB \rlap{\ ,}
%      \end{tikzcd} \]
%    by defining the solution $\widetilde{h} : \CC \to \CA$ by induction on the length of the objects of $\CC$.
% \end{proof}

All type-theoretic structures can be transported along contextual isomorphisms.
Thus, given a morphism $F : \CMod_{\Th}(\CC \to \CD)$ of models of some theory $\Th$, the contextual image $\cxlim F$ carries a canonical structure of model of $\Th$, and the factors $\CC \to \cxlim F$ and $\cxlim F \to \CD$ are both morphisms of models of $\Th$.

If $\Th$ is a type theory signature, the category of contextual models of $\Th$ is written $\CMod_{\Th}^{\cxl}$.
We have an adjunction
\[ \begin{tikzcd}
    \CMod_{\Th}^{\cxl} \ar[r, bend right] \ar[r, phantom, "\bot"] & \CMod_{\Th} \rlap{\ .} \ar[l, bend right]
  \end{tikzcd} \]
The right adjoint $\CMod_{\Th}^{\cxl} \to \CMod_{\Th}$ is just the functor forgetting that a model is contextual.
The left adjoint $\CMod_{\Th} \to \CMod_{\Th}^{\cxl}$ takes the contextual core of a model.

\begin{prop}
  The contextual core of a model $\CC : \CMod_{\Th}$ is the initial model of $\Th$ equipped with a contextual isomorphism into $\CC$.
\end{prop}
\begin{proof}
  Given any other model $\CD$ equipped with a contextual isomorphism $F : \CD \to \CC$, we have, since $\Init_{\Th} \to \cxl \CC$ is a contextual extension, a unique lift in the following diagram.
  \[ \begin{tikzcd}
      \Init_{\Th} \ar[d] \ar[r] & \CD \ar[d, "F"] \\
      \cxl \CC \ar[ru, dashed] \ar[r] & \CC
    \end{tikzcd} \]
\end{proof}

\begin{prop}\label{prop:contextual_section}
  To check that a CwF $\CC$ is contextual, it suffices to check that the morphism $\cxl \CC \to \CC$ admits a section.
\end{prop}
\begin{proof}
  Assume that $r : \cxl \CC \to \CC$ admits a section $s : \CC \to \cxl \CC$.
  Then the following diagram commutes.
  \[
    \begin{tikzcd}
      \cxl \CC \ar[rd, "\id", shift left] \ar[rd, "r \cdot s"', shift right] \ar[rr, "r"] && \CC \\
      & \cxl \CC \ar[ru, "r"']&
    \end{tikzcd} \]
  Since $\cxl \CC$ is initial among the CwFs with a contextual isomorphism into $\CC$, we have that $r \cdot s = \id$, and $r : \cxl \CC \to \CC$ is therefore an isomorphism, as needed.
\end{proof}

\begin{prop}
  For any type theory signature $\Th$, the initial model $\Init_{\Th} : \CMod_{\Th}$ is contextual.
\end{prop}
\begin{proof}
  By initiality of $\Init_{\Th}$, the morphism $\cxl \Init_{\Th} \to \Init_{\Th}$ admits a section, which implies that $\Init_{\Th}$ is contextual by \cref{prop:contextual_section}.
\end{proof}

\begin{defi}
  Given a model $\CC$ of a theory $\Th$ and $\Gamma : \abs{\CC}$, the \defemph{contextual slice} $(\CC \sslash \Gamma)$ is defined to be the contextual core of the slice model $(\CC / \Gamma)$.
  \defiEnd
\end{defi}

\subsection{Join of families and telescopes}

We will not assume the presence of $\Sigma$-types in our type theories.
To circumvent their absence in some constructions, we will need to work with families of telescopes, whose types and terms are finite sequences of types and terms of the base family.
It is convenient to present them as the coproduct of length $n$ telescopes for all $n : \Nat$, and to generalize the notion of length $n$ telescope to a more heterogeneous notion, using the notion of join of families\footnote{The author learnt of this presentation from Christian Sattler.}.

We work internally to some presheaf category $\CPsh\ \CC$.

\begin{defi}
  Let $\MC = (\Ty_{\MC},\Tm_{\MC})$ and $\MD = (\Ty_{\MD},\Tm_{\MD})$ be two internal families (not necessarily representable).
  Their \defemph{join} $\MC \ast \MD$ is the internal family defined by:
  \begin{alignat*}{2}
    &\Ty_{\MC \ast \MD} && \triangleq (A : \Ty_{\MC}) \times (B : \Tm_{\MC}\ A \to \Ty_{\MD}) \\
    & \Tm_{\MC \ast \MD}\ (A, B) && \triangleq (a : \Tm_{\MC}\ A) \times (b : \Tm_{\MD}\ (B\ a))
  \end{alignat*}
  Whenever both $\MC$ and $\MD$ are representable, the family $\MC \ast \MD$ is also representable (since locally representable presheaves are closed under $\Sigma$-types).
  \defiEnd
\end{defi}

In other words, the join of $\MC$ and $\MD$ is the family of length $2$ telescopes, whose first and second components come respectively from $\MC$ and $\MD$.

\begin{defi}
  If $\MC$ is an internal family and $n : \Nat$, the family $\MC^{\ast n}$ of \defemph{length $n$ telescopes} is the $n$-fold iterated join of $\MC$.
  \defiEnd
\end{defi}

\begin{defi}
  If $\MC$ is an internal family, the family of \defemph{telescopes} of $\MC$ is the coproduct $\MC^{\star} \triangleq \coprod_{n : \Nat}\ \MC^{\ast n}$.
  We write $\Ty_{\MC}^{\star}$ and $\Tm_{\MC}^{\star}$ for the components of $\MC^{\star}$.
  If $\MC$ is representable, then $\MC^{\star}$ is also representable.
  \defiEnd
\end{defi}

If $\CC$ is a contextual CwF, we may identify its objects with the closed telescopes of types (i.e. the global elements of $\Ty^{\star}_{\CC}$) and its morphisms from $\Gamma$ to $\Delta$ with the natural transformations from $\Tm^{\star}_{\CC}\ \Gamma$ to $\Tm^{\star}_{\CC}\ \Delta$. When $\CC$ is an arbitrary CwF, this is an explicit description of the objects and morphisms of the contextual core of $\CC$.

\subsection{Cumulative categories with families}

We will actually work with type theories that extend the theory of cumulative categories with families, rather than the simpler theory of categories with families.
Cumulative categories with families were introduced by Coquand~\cite{CoquandNbE} to describe universe hierarchies.
Working with cumulative CwFs, instead of mere CwFs, ensures that every type admits a code in some universe.
In presence of identity types, this provides in turn a way to compare types up to internal equality of codes.

This is mainly for convenience: most of our results could also be formulated and proven for mere CwFs, comparing types up to equivalence.
However, using cumulative CwFs simplifies the proofs and the presentation.

\begin{defi}
  Internally to a presheaf model $\CPsh_{\CC}$, an internal cumulative family consists of a family
  \[\Ty : \Nat \to \SPsh_{\CC}\]
  of presheaves of types ($\Ty_{n}$ is the presheaf of types in the $n$-th universe of the hierarchy), a family
  \[\Tm : \{n : \Nat\} \to \Ty_{n} \to \SRepPsh_{\CC} \]
  of locally representable presheaves of terms, lifting functions
  \[ \mathsf{Lift}_{\Ty} : \Ty_{n} \to \Ty_{n+1}\]
  and isomorphisms
  \[ \mathsf{lift}_{\Tm} : \Tm_{n}\ A \simeq \Tm_{n+1}\ (\mathsf{Lift}_{\Ty}\ A) : \mathsf{lower}_{\Tm}. \]
  Given an internal cumulative family $\MC = (\Ty, \Tm)$, we write $\MC^{n}$ for the internal family $(\Ty_{n}, \Tm_{n})$.
 
  A cumulative CwF, or cCwF, is a category $\CC$ equipped with a global cumulative family $(\Ty, \Tm)$.
  \defiEnd
\end{defi}

\begin{defi}
  The structure of cumulative universes over a cumulative internal family $(\Ty,\Tm)$ consists of operations \[ \UU : (n : \Nat) \to \Ty_{n+1} \] along with isomorphisms \[ \Tm_{n+1}\ \UU_{n} \simeq \Ty_{n}, \]
  that we will leave implicit.
  \defiEnd
\end{defi}

This differs slightly from Coquand's definition of cumulative CwFs.
Coquand requires the natural transformations $\mathsf{Lift}_{\Ty} : \Ty_{n} \to \Ty_{n+1}$ to be injective, and uses equalities $\Tm\ \UU_{n} = \Ty_{n}$ instead of isomorphisms.

When talking about type theory signatures in this paper, we mean signatures over the theory of cumulative CwFs with universes, i.e. extensions of the signature of cumulative CwFs with universes by new operations and equations only (no new sorts).

The notion of contextuality generalizes straightforwardly to cumulative CwFs.
For instance, a contextual isomorphism between cumulative CwFs is a morphism that is bijective on types and terms for each universe level.

The notion of telescope can also be adapted to cumulative families.
Given an internal cumulative family $\MC$ and a list $w = (w_{1}, \cdots, w_{n})$ of natural numbers, the family of $w$-shaped telescopes is the join $\MC^{\ast w} \triangleq \MC^{w_{1}} \ast \dots \ast \MC^{w_{n}}$.
The family of all telescopes of $\MC$ is the coproduct $\MC^{\star} \triangleq \coprod_{w : \Nat^{\star}}\ \MC^{\ast w}$

For many of the properties of cumulative CwFs with universes and morphisms of cCwFs that are defined by conditions on types and terms, the condition on types is a consequence of the condition on terms of the corresponding universe.
For instance, to check that a morphism is a contextual isomorphism, it is sufficient to check that it is bijective on terms for each universe level.

To improve the readability, we will leave the universe levels implicit in most constructions and proofs.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
