\section{Equivalences between type theories}\label{sec:equivalence_models}

In this section, we discuss the notion of Morita equivalence between a weak type theory $\Th_{w}$ and a strong type theory $\Th_{s}$.
They have been introduced as the weak equivalences of a model structure on a category of type theories in \cite{IsaevMorita}.
While \cite{IsaevMorita} considers arbitrary morphisms between type theories, we only consider extensions of type theories by additional strict equalities.

\subsection{Equational extensions}

We fix a type theory signature $\Th_{w}$ over the theory of cumulative CwFs with universes and weak identity types.
\begin{defi}
  A \defemph{marked equation} over $\Th_{w}$ consists of a finitely generated cellular model $\Init_{w}[X]$, along with a closed internal equality $p : \abs{\Tm_{\Init_{w}[X]}}_{\diamond}\ (\Id\ \{A\}\ a\ b)$ of $\Init_{w}[X]$.

  The equation is said to hold strictly in a model $\CC$ of $\Th_{w}$ if for every object $\Gamma : \abs{\CC}$ and morphism $F : \Init_{w}[X] \to (\CC \sslash \Gamma)$, $F$ maps $p$ to the reflexivity equality.

  An \defemph{equational extension} of $\Th_{w}$ is a family of marked equations over $\Th_{w}$.
  \defiEnd
\end{defi}

If we were to compare types up to equivalence instead of internal equality of codes, the definition of marked equation would need to be extended to also include marked type equivalences.

We give some examples of equational extensions.
\begin{exas} \hfill
  \begin{enumerate}
    \item For the extensions from weak computation rules to strict computation rules, we mark the computation rules that should be made strict.
      For example, in the case of identity types, we mark the family of internal equalities $\J_{\beta}\ P\ d : \Id\ (\J\ P\ d\ \refl)\ d$.
      In the case of $\Pi$-types, we mark the internal equalities $\app_{\beta} : \Id\ (\app\ (\lam\ b)\ a)\ (b\ a)$ and $\funext_{\beta} : \Id\ (\funext\ f\ f\ (\lam\ (a \mapsto \refl)))\ \refl$ (and perhaps
      $\funext\text{-}\app_{\beta}$ as well).
    \item When considering the extension from inductive natural numbers to natural numbers with a strictly associative addition, we proceed in in two steps.
      First we extend the base theory by adding
      \[ \mathsf{plus} : \Tm\ \Nat \to \Tm\ \Nat \to \Tm\ \Nat \]
      as a new primitive operation, along with some of the internal equalities that it satisfies, such as
      \[ \mathsf{plus}_{0} : \{x\} \to \Tm\ (\Id\ (\mathsf{plus}\ 0\ x)\ x), \]
      \[ \mathsf{plus}_{1} : \{x\} \to \Tm\ (\Id\ (\mathsf{plus}\ x\ 0)\ x), \]
      \[ \mathsf{plus}_{2} : \{x\} \to \Tm\ (\Id\ (\mathsf{plus}\ (\mathsf{plus}\ x\ y)\ z)\ (\mathsf{plus}\ x\ (\mathsf{plus}\ y\ z))), \] etc.
      The operation $\mathsf{plus}$ is homotopic to the usual inductively defined addition, but not strictly equal to it.
      This kind of extension is conservativive.
      The weak type theory $\Th_{w}$ is then this extended theory.

      As a second step, we consider the equational extension of that theory obtained by marking the equalities $\mathsf{plus}_{0}$, $\mathsf{plus}_{1}$, $\mathsf{plus}_{2}$, etc.
      Thus the strong type theory $\Th_{s}$ includes the strict equalities $\mathsf{plus}\ 0\ x = x$, $\mathsf{plus}\ x\ 0 = x$, $\mathsf{plus}\ (\mathsf{plus}\ x\ y)\ z = \mathsf{plus}\ x\ (\mathsf{plus}\ y\ z)$, etc.
      It also includes the strict equalities $\mathsf{plus}_{0} = \refl$, $\mathsf{plus}_{1} = \refl$, $\mathsf{plus}_{2} = \refl$, etc.

    \item To consider the extension of a type theory with a new universe of strict propositions, we would also perform two steps.
      As a first step, we introduce a new constant type $\mathsf{SProp}$, along with an equality in $\Id\ \mathsf{SProp}\ \mathsf{HProp}$ with the universe $\mathsf{HProp}$ of propositions.
      We write $F : \mathsf{SProp} \to \mathsf{HProp}$ for the associated transport function.

      Secondly, we mark the family of equations
      \[ (A : \Tm\ \mathsf{SProp}) (a , b : \Tm\ (F\ A)) \to \Tm\ (\Id\ a\ b). \]

      In the resulting strong type theory, the only way to obtain closed elements of $\mathsf{SProp}$ is to use the inverse of the equivalence $F$ to replace elements of $\mathsf{HProp}$ by elements in $\mathsf{SProp}$.

      Note that the equational extension that marks instead the family of equations
      \[ (A : \Tm\ \mathsf{HProp}) (a , b : \Tm\ A) \to \Tm\ (\Id\ a\ b) \]
      is not a conservative extension in the absence of UIP.
      Indeed, as remarked in \cite{SProp}, if all propositions are strict propositions, then UIP holds.

    \item As a last example, we can also mark the family of all equalities
      \[ (A : \Ty)\ (x, y : \Tm\ A)\ (p : \Tm\ (\Id\ \{A\}\ x\ y)) \mapsto p. \]
      The corresponding strong type theory then includes the equality reflection rule.
  \end{enumerate}
\end{exas}

\subsection{Equivalences of theories}

We now work with a fixed choice of weak type theory $\Th_{w}$ and equational extension $\Th_{e}$.
The strong type theory $\Th_{s}$ is then defined as the extension of $\Th_{w}$ by the strict equalities $x = y$ and $p = \refl$ for every internal equality $p : \Id\ x\ y$ marked in $\Th_e$.

We have an adjunction between the categories $\CMod_{w}$ of models of $\Th_{w}$ and $\CMod_{s}$ of models of $\Th_{s}$.
\begin{center}\(\begin{tikzcd}
    \CMod_{w} \ar[r, swap, bend right, "L_{s}"] \ar[r, phantom, "\top"] & \CMod_{s} \ar[l, swap, bend right, "R_{s}"]
  \end{tikzcd}\)\end{center}
As $\Th_{s}$ is an equational extension of $\Th_{w}$, the functor $R_{s} : \CMod_{s} \to \CMod_{w}$ is simply the fully faithful forgetful functor that forgets that a strong model satisfies the additional equations. We will often omit $R_{s}$, and simply see any object of $\CMod_{s}$ as an object of $\CMod_{w}$. The left adjoint $L_{s} : \CMod_{w} \to \CMod_{s}$ can be shown to exist by various methods. One possibility is to use the adjoint functor theorem, using the fact that $\CMod_{w}$ and $\CMod_{s}$ are locally finitely presentable and that $R_{s}$ preserves limits.

The left adjoint can also be computed from the presentation of a model $\CC : \CMod_{w}$ by generators and relations. Such a presentation can be obtained from the cellular replacement of the model $\CC$ by some cellular model $\Init_{w}[X]$. Since left adjoints preserve colimits and cellular models are built by iterated pushouts, $L_{s}\ \Init_{w}[X] \simeq \Init_{s}[X]$, where $\Init_{s}[X]$ is the cellular strong model with the same generators as $\Init_{w}[X]$. Since $\CC$ is a quotient of $\Init_{w}[X]$ by a fibrant congruence and left adjoints preserve quotients, $L_{s}\ \CC$ is also the quotient of $\Init_{s}[X]$ by some congruence, although that congruence may fail to be fibrant in general.

We write $\eta^{X} : \Init_{w}[X] \to \Init_{s}[X]$ for the unit of this adjunction at a cellular model $\Init_{w}[X]$.

\begin{defi}\label{def:th_weq}
  We say that $\Th_{w}$ and $\Th_{s}$ are Morita equivalent if for every cofibrant contextual model $\CC : \CMod_{w}^{\cof}$ of $\Th_{w}$, the unit $\eta_{\CC} : \CMod_{w}(\CC \to R_{s}\ (L_{s}\ \CC))$ is a
  weak contextual equivalence.
  \defiEnd
\end{defi}

It is shown in \cite{IsaevMorita} that whenever $\Th_{w}$ is semi-model, then is weakly equivalent to $\Th_{s}$ if and only if $\Th_{s}$ is also semi-model and the adjunction $(L_{s} \dashv R_{s})$ is a Quillen equivalence.

We now show that in order to prove that $\Th_{w}$ and $\Th_{s}$ are equivalent, it is sufficient to look at the cellular models of $\Th_{w}$.
Recall that the cellular models of $\Th_{w}$ are very similar to the initial model of $\Th_{w}$.
Thus, for most type theories, whenever we can prove that the initial models of $\Th_{w}$ and $\Th_{s}$ are equivalent, we can expect the same methods to work for arbitrary cellular models, implying that $\Th_{w}$ and $\Th_{s}$ are equivalent.
\begin{prop}\label{prop:weq_theories_char}
  The following conditions are equivalent:
  \begin{enumerate}
    \item\label{item:weq_theories_char_1} The theories $\Th_{w}$ and $\Th_{s}$ are Morita equivalent
    \item\label{item:weq_theories_char_2} The condition of \cref{def:th_weq} holds for every cellular model of $\Th_{w}$.
    \item\label{item:weq_theories_char_3} The condition of \cref{def:th_weq} holds for every finite cellular model of $\Th_{w}$.
  \end{enumerate}
\end{prop}
\begin{proof} The forward implications trivially hold. We show the reverse implications.
   \begin{description}
     \item[(\ref{item:weq_theories_char_2} $\Ra$ \ref{item:weq_theories_char_1})]
       Take a cofibrant contextual model $\CC$. It is the retract of some cellular model $\Init_{w}[X]$. Then $L_{s}\ \CC$ is also a retract of $L_{s}\ \Init_{w}[X]$, and furthermore $\eta_{\CC} : \CC \to L_{s}\ \CC$ is a retract of $\eta^{X} : \Init_{w}[X] \to L_{s}\ \Init_{w}[X]$.
       Since weak contextual equivalences are closed under retracts and $\eta^{X}$ is a weak contextual equivalence by assumption, $\eta_{\CC} : \CC \to L_{s}\ \CC$ is also a weak contextual equivalence.
     \item[(\ref{item:weq_theories_char_3} $\Ra$ \ref{item:weq_theories_char_2})]
       For this we rely on some well-known properties of locally finitely presentable categories and freely generated models that we do not prove in this paper, since the proofs are quite lengthy, and not required for the main results of this paper. The idea is that since we consider finitary type theories, any type or term of a freely generated model is supported by a finite subset of generators. \\
       Let $\Init_{w}[X]$ be a cellular model of $\Th_{w}$. We know that $\Init_{w}[X]$ is the filtered colimit $\colim\limits_{Y \rat X} \Init_{w}[Y]$ of its finite cellular subextensions. Since left adjoints preserve colimits, $\Init_{s}[X]$ can be computed as the filtered colimit $\colim\limits_{Y \rat X} \Init_{s}[Y]$. For every type $A$ or term $a$ of $\Init_{s}[X]$, there merely exists a finite cellular subextension $Y \rat X$ such that the type or term already exists in $\Init_{s}[Y]$. Using condition \ref{item:weq_theories_char_3}, we can then compute a lift of $A$ or $a$ in $\Init_{w}[Y]$.
   \end{description}
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
