\documentclass[aspectratio=169]{beamer}
\beamertemplatenavigationsymbolsempty
\usetheme{metropolis}
\usecolortheme{orchid}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{palatino}
% \usepackage{mathpazo}

\usepackage{todonotes}
\presetkeys{todonotes}{inline}{}

\usepackage{tikz}
\usetikzlibrary{cd}
\usetikzlibrary{decorations.pathmorphing}

\usepackage{xcolor}
\usepackage{multirow}
\usepackage{marginnote}

\usepackage{mathpartir}

\usepackage{fontawesome}

\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{environ}
\usepackage{bm}

\newcommand{\defemph}[1]{\textbf{#1}}
\newcommand{\type}{\mathsf{type}}
\newcommand{\Init}{\mathbf{0}}

\newcommand{\Th}{\mathbb{T}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}

\newcommand{\CMod}{\mathbf{Mod}}
\newcommand{\CA}{\mathcal{A}}
\newcommand{\CB}{\mathcal{B}}
\newcommand{\CC}{\mathcal{C}}
\newcommand{\CD}{\mathcal{D}}
\newcommand{\CE}{\mathcal{E}}
\newcommand{\CF}{\mathcal{F}}
\newcommand{\CG}{\mathcal{G}}
\newcommand{\CH}{\mathcal{H}}
\newcommand{\CL}{\mathcal{L}}
\newcommand{\CM}{\mathcal{M}}
\newcommand{\CN}{\mathcal{N}}
\newcommand{\CR}{\mathcal{R}}
\newcommand{\CS}{\mathcal{S}}
\newcommand{\CV}{\mathcal{V}}
\newcommand{\CW}{\mathcal{W}}

\newtheorem*{thm*}{Theorem}
\newtheorem*{defi*}{Definition}
\newtheorem*{prop*}{Proposition}
\newtheorem*{sdefi*}{Definition (simplified)}

\title{Coherence of definitional equalities in type theory}
\author{Rafaël Bocquet%~\inst{1}
}
% \institute
% {
%   \inst{1}Eötvös Loránd University, Budapest, Hungary
%}
\date{\tiny Workshop on Homotopy Type Theory / Univalent Foundations, July 5, 2020}
% \thanks{The author was supported by the European Union, co-financed by the European Social Fund (EFOP-3.6.3-VEKOP-16-2017-00002). }

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Definitional equalities}


  \begin{columns}
    \begin{column}{0.45\textwidth}
      \begin{center}
        An internal equality: \\
        $- : \mathsf{Id}\ (0 + n)\ n$
      \end{center}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{center}
        A definitional equality: \\
        $n + 0 = n$
      \end{center}
    \end{column}
  \end{columns}

  \hfill
 
  More definitional equalities $\rightsquigarrow$
  \begin{tabular}[t]{@{}l@{}}Nicer internal constructions and proofs \\
    Automatically coherent \\
    Less models\end{tabular}

  \hfill

  \begin{center}
    Which equalities can be made definitional ?
  \end{center}

\end{frame}

\begin{frame}
  \frametitle{Conservativity}


  Weak type theory $\Th_{w}$. \\
  Family of weak equalities $\Th_{e}$. \\
  Strong type theory $\Th_{s}$ = extension of $\Th_{w}$ in which the equalities of $\Th_{e}$ are strict.

  \begin{center}
    When is $\Th_{s}$ conservative over $\Th_{w}$ ?
  \end{center}

  \begin{theorem}[Hofmann's conservativity theorem]
    Extensional Type Theory is conservative over Intensional Type Theory with \textbf{Uniqueness of Identity Proofs}.
  \end{theorem}

  \begin{center}
    This talk: methods to prove conservativity without UIP.
  \end{center}

\end{frame}

\begin{frame}
  \frametitle{Examples: Weak computation rules}
  Theory of weak identity types:
  \begin{mathpar}
    \inferrule{[y:A, p : \mathsf{Id}_{A}\ x\ y]\ P(y, p)\ \mathsf{type} \\ d : P(x,\mathsf{refl}_{x}) \\\\ y : A \\ p : \mathsf{Id}_{A}\ x\ y}
    {\mathsf{J}\ P\ d\ p : P(y, p)}

    \inferrule{[y:A, p : \mathsf{Id}_{A}\ x\ y]\ P(y,p)\ \mathsf{type} \\ d : P(x,\mathsf{refl}_{x})}
    {\mathsf{J}_{\beta}\ P\ d : \mathsf{Id}_{}\ (\mathsf{J}\ P\ d\ \mathsf{refl}_{x})\ d}
  \end{mathpar}

  Extension to strong identity types:
  \begin{mathpar}
    \mathsf{J}\ P\ d\ \mathsf{refl}_{x} = d

    \mathsf{J}_{\beta}\ P\ d = \mathsf{refl}_{d}
  \end{mathpar}

  Similar: weak $\Pi$-types, weak $\Sigma$-types, weak natural numbers, weak universes, ...
\end{frame}

\begin{frame}
  \frametitle{Examples: Strictly associative operations}
  Strictly associative addition of natural numbers.
  \begin{mathpar}
    0 + x = x = x + 0

    x + (y + z) = (x + y) + z
  \end{mathpar}

  Strictly associative composition of paths.
  \begin{mathpar}
    \mathsf{refl} \cdot p = p = p \cdot \mathsf{refl}

    p \cdot (q \cdot r) = (p \cdot q) \cdot r
  \end{mathpar}
\end{frame}

\begin{frame}
  \frametitle{Examples: Universes of strict algebraic structures}

  Strict propositions:
  \begin{mathpar}
    |-| : \mathsf{SProp} \simeq \mathsf{Prop}

    \inferrule{P : \mathsf{SProp} \\ x, y : |P|}{x = y}
  \end{mathpar}

  Strict rings:
  \begin{mathpar}
    |-| : \mathsf{SRing} \simeq \mathsf{Ring}
    \\\\
    (-)[\bm{X}] : \mathsf{SRing} \to \mathsf{SRing}
   
    (-)[1/-] : (R : \mathsf{SRing}) \to |R| \to \mathsf{SRing}
  \end{mathpar}

  % ``Strict'' pointed types:
  % \begin{mathpar}
  %   |-| : \mathsf{SPtType} \simeq \mathsf{PtType}

  %   - \wedge - : \mathsf{SPtType} \to \mathsf{SPtType} \to \mathsf{SPtType}
  % \end{mathpar}
\end{frame}

\begin{frame}
  \frametitle{Homotopy theory of type theories}
  \textbf{(Kapulkin and Lumsdaine, 2016)} and \textbf{(Isaev, 2016)} construct (left semi-) model structures on the categories of models of type theories.
  \begin{defi*}
    A morphism $F : \CC \to \CD$ of models is
    \begin{itemize}
      \item a \defemph{trivial fibration} if it is surjective on types and terms
      \item a \defemph{weak equivalence} if it is surjective up to equivalences on types and propositional equality on terms
      \item a \defemph{fibration} if it satisfies path and equivalence lifting properties.
    \end{itemize}
  \end{defi*}
  Hofmann's conservativity theorem: the map $\Init_{ITT} \to \Init_{ETT}$ is a trivial fibration.
\end{frame}

\begin{frame}
  \frametitle{Homotopy theory of type theories}
  \begin{theorem}[\textbf{Kapulkin and Lumsdaine, 2016}]
    The classes of weak equivalences, fibrations and cofibrations define a left semi-model structure on $\CMod^{\mathsf{cxl}}_{\mathsf{Id},\Sigma,(\Pi)}$.
  \end{theorem}

  This essentially says that the equivalences and propositional equalities are well-behaved.

  We say that a theory $\Th$ is semi-model if it satisfies this condition.

  % This condition is satisfied for any type theory with enough univalent universes and $\Pi$-types (with a strict $\beta$ rule).
\end{frame}

\begin{frame}[fragile]
  \frametitle{Equivalences between type theories}
  \[ \begin{tikzcd}
      \CMod_{w} \ar[r, phantom, "\bot"] \ar[r, bend left, "L_{s}"] &
      \CMod_{s} \ar[l, bend left, "R_{s}"]
    \end{tikzcd} \]
  \begin{defi*}[Isaev, 2018]
    The theories $\Th_{w}$ and $\Th_{s}$ are weakly equivalent if for every cofibrant model $\CC$ of $\Th_{w}$, the unit $\eta_{\CC} : \CC \to L_{s}\ \CC$ is a weak equivalence.
  \end{defi*}
  Every model $\CC$ admits a cofibrant replacement $\CC^{\mathsf{cof}}$.
  \[
    \begin{tikzcd}
      \CC & \CC^{\mathsf{cof}} \ar[l, "{\sim}"'] \ar[r, "{\sim}"] & L_{s}\ \CC^{\mathsf{cof}}
    \end{tikzcd}
  \]
\end{frame}

\begin{frame}[fragile]
  \frametitle{Mac Lane's coherence theorem for monoidal categories}
  \[ \begin{tikzcd}
      \mathbf{WkMonCat} \ar[r, phantom, "\bot"] \ar[r, bend left, "L"] &
      \mathbf{StrMonCat} \ar[l, bend left, "R"]
    \end{tikzcd} \]

  \begin{thm*}
    For every weak monoidal category $\CC : \mathbf{WkMonCat}$, the unit $\eta_{\CC} : \CC \to L\ \CC$ is a monoidal equivalence.
  \end{thm*}
\end{frame}

\begin{frame}
  \frametitle{Cellular models}
  Cellular model $\Init_{w}[X]$ of $\Th_{w}$: model of $\Th_{w}$ freely generated by some types and terms.
  (e.g. $\Init_{w}[\bm{A} : \Ty]$, $\Init_{w}[\bm{A} : \Ty, \bm{a} : \Tm\ \bm{A}]$)

  (Same notation as for a freely generated ring $\mathbb{Z}[\bm{x_{1}}, \cdots, \bm{x_{n}}]$)

  The theories $\Th_{w}$ and $\Th_{s}$ are equivalent iff every cellular model $\Init_{w}[X]$ of $\Th_{w}$ is equivalent to the strong model $L_{s}\ \Init_{w}[X] = \Init_{s}[X]$ generated by the same data.
\end{frame}

\begin{frame}
  \frametitle{Fibrant congruences}

  Congruence: equivalence relations on types and terms, compatible with all operations.
  \begin{mathpar}
    (\Ty,\sim) : \mathsf{Setoid}

    (\Tm,\sim) : \mathsf{SetoidFam}\ (\Ty,\sim)
  \end{mathpar}

  Fibrant congruence: $(\Tm,\sim) : \mathsf{SetoidFam}\ (\Ty,\sim)$ should be a fibrant setoid family.

  \begin{prop*}
    The trivial fibrations are, up to (contextual) isomorphism, the quotients of models by fibrant congruences.
  \end{prop*}

  Proof of Hofmann's conservativity theorem: construct some congruence over $\Init_{ITT}$ and show that it is fibrant.
\end{frame}

\begin{frame}
  \frametitle{(Type-theoretic) higher congruences}
  Congruence over $\CC$: extension of $\CC$ to a model valued in setoids.

  Higher congruence over $\CC$: extension of $\CC$ to a model valued in $\infty$-groupoids.

  \pause

  Brunerie's type-theoretic definition of $\infty$-groupoid: a weak $\infty$-groupoid is (essentially) a model of some type theory.

  We will define a type theory $\Th_{w,2}$ extending $\Th_{w}$. \\
  Set-valued models of $\Th_{w,2}$ correspond to $\infty$-groupoid valued models of $\Th_{w}$.

  % \begin{defi*}
  %   A type-theoretic higher congruence over a model $\CC$ of $\Th_{w}$ is a weak equivalence $\iota : \CC \to \widetilde{\CC}$ where $\widetilde{\CC}$ is a model of the type theory $\Th_{w,2}$.
  % \end{defi*}
\end{frame}

\begin{frame}
  \frametitle{The type theory $\Th_{w,2}$ (1/2)}
  Extension of $\Th_{w}$ by:
  \begin{itemize}
    \item An outer level:
      \begin{mathpar}
        \inferrule{ }{- : \Ty^o}

        \inferrule{A : \Ty^o}{- : \Tm^o\ A}
      \end{mathpar}

    \item such that the inner types and terms are strictly classified by some outer types:
      \begin{mathpar}
        \inferrule{ }{\mathsf{ty} : \Ty^o}

        \inferrule{ }{\Tm^o\ \mathsf{ty} = \Ty}
        \\\\
        \inferrule{A : \Ty}{\mathsf{tm}\ A : \Ty^o}

        \inferrule{A : \Ty}{\Tm^o\ (\mathsf{tm}\ A) = \Tm\ A}
      \end{mathpar}
    \item ...
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The type theory $\Th_{w,2}$ (2/2)}
  \begin{itemize}
    \item ...
    \item Weak outer identity types:
      \begin{mathpar}
        \inferrule{A : \Ty^{o} \\ x : \Tm^o A \\ y : \Tm^o\ A}{\mathsf{Id}^{o}_{A}\ x\ y : \Ty^o}
      \end{mathpar}

    \item Outer $\Pi$-types with arities in inner types:
      \begin{mathpar}
        \inferrule{A : \Ty \\ [a : A]\ B(a) : \Ty^o}{\Pi^o\ A\ B : \Ty^o}
      \end{mathpar}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Lifting the equalities of $\Th_{e}$ to the outer layer}

  Further extension $\Th_{w,2,e}$. \\
  $\Init_{w,2,e}$: higher congruence over $\Init_{w}$ freely generated by the equalities of $\Th_{e}$.


  Example: if the following equality is marked in $\Th_{e}$
  \[ \mathsf{J}_{\beta} : \Tm\ (\mathsf{Id}\ (\mathsf{J}\ P\ d\ \mathsf{refl})\ d), \]

  we add to $\Th_{w,2,e}$:
  \begin{mathpar}
    \widehat{\mathsf{J}_{\beta}} : \Tm^o\ (\mathsf{Id}^o\ (\mathsf{J}\ P\ d\ \mathsf{refl})\ d)

    \begin{tikzcd}
      \mathsf{J}\ P\ d\ \mathsf{refl} \ar[d, dash, "\widehat{\mathsf{J}_\beta}"'] \ar[r, dash, "\mathsf{J}_\beta"] \ar[rd, phantom, "\widetilde{\mathsf{J}_{\beta}}"]& d \ar[d, equal] \\
      d \ar[r, equal] & d
    \end{tikzcd}
  \end{mathpar}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Main diagram}
  \[  \begin{tikzcd}
      \Init_{w} \ar[rr] \ar[rd] && \Init_{s} \\
      & \Init_{w,2,e} \ar[ru] &
    \end{tikzcd} \]

  If both $\Init_{w} \to \Init_{w,2,e}$ and $\Init_{w,2,e} \to \Init_{s}$ are weak equivalences, then so is $\Init_{w} \to \Init_{s}$.

  \pause
 
  If $\Th_{w}$ is semi-model, then $\Init_{w} \to \Init_{w,2,e}$ is a weak equivalence (up to some issues with the stability of $\mathsf{J}^{o}$ under substitution).

  \pause

  If $\Init_{w,2,e}$ is \emph{acyclic}, then $\Init_{w,2,e} \to \Init_{s}$ is a trivial fibration.
\end{frame}

\begin{frame}[t]
  \frametitle{Acyclicity}

  \begin{definition}
    A model $\CC$ of $\Th_{w,2}$ is \defemph{acyclic} if
    \begin{itemize}
      \item the outer type $\mathsf{ty} : \Ty^{o}$ is $0$-truncated (with respect to the outer identity types);
      \item for any inner type $A : \Ty$, the outer type $\mathsf{tm}\ A : \Ty^{o}$ is $0$-truncated.
    \end{itemize}
  \end{definition}

  If a model $\CC$ is \emph{acyclic}, we can quotient by the outer equalities.

  \pause
 
  \begin{center}
    How do we prove acyclicity ?
  \end{center}

  \pause

  Parametricity translation of the outer layer (similar to \textbf{(Lasson, 2014)}). \\
  Weak normalization for the inner layer.
\end{frame}

\begin{frame}
  \frametitle{Conclusions}

  Heuristic: if the internal equality of the weak type theory $\Th_{w}$ are well-behaved ($\Th_{w}$ is semi-model) and the definitional equality of the strong type theory $\Th_{s}$ is well-behaved ($\Th_{s}$ is normalizing), then $\Th_{s}$ should be conservative over $\Th_{w}$.

  % Can we use the same methods to prove coherence theorems outside of type theory ?

  How do we prove the conservativity of non-equational extensions (e.g. the conservativity of CTT over HoTT) ?
\end{frame}

\end{document}
